/*
 * json_params.h
 *
 *  Created on: Jun 10, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef JSON_PARAMS_H_
#define JSON_PARAMS_H_

#include <sstream>
#include <string>
#include <iomanip>
#include <fstream>
#include <iostream>

#include "jsonLib/json.hpp"
#include "jparam_structs.h"

namespace json_params {

class JsonParamsHandler {
public:
	JsonParamsHandler() = default;

	JsonParamsHandler(const std::string& filename) {
		std::ofstream file_stream { };
		file_stream.open(filename, std::ofstream::in | std::ofstream::app);
		std::stringstream buffer { };
		buffer << file_stream.rdbuf();
		auto fileContent = buffer.str();
		if (fileContent.empty()) {
			std::cerr << "ERROR: Params file not found: " << filename << std::endl;
			exit(1);
		}
		params = json::JSON::Load(fileContent);
	}

	JsonParamsHandler(const std::string& fileContent, int dummy) {
		params = json::JSON::Load(fileContent);
	}

	template<typename P>
	typename P::Type get() const {
		if (params.hasKey(P::name)) {
			return getValue<typename P::Type>(P::name);
		} else {
			return P::getDefault();
		}
	}
	template<typename P>
	void set(typename P::Type value) {
		setValue<P>(value);
	}

	void dump(const std::string& filename) const {
		std::ofstream out(filename);
		out << params.dump();
		out.close();
	}

	void overrideWith(const json::JSON& overrides) {
		for (auto kv : overrides.ObjectRange()) {
			params[kv.first] = kv.second;
		}
	}

protected:
	template<typename T>
	T getValue(const std::string& key) const;

	template<typename P>
	void setValue(typename P::Type value) {
		params[P::name] = value;
	}

	double getFractional(const json::JSON& json, bool& ok) const {
		auto res = json.ToFloat(ok);
		if (!ok) {
			res = json.ToInt(ok);
		}
		return res;
	}

	json::JSON params { };
};

template<>
inline double JsonParamsHandler::getValue(const std::string& key) const {
	bool ok = false;
	auto res = getFractional(params[key], ok);
	if (ok)
		return res;
	//the type of this value is not numeric -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, Double expected" << std::endl;
	exit(1);
}

template<>
inline float JsonParamsHandler::getValue(const std::string& key) const {
	bool ok = false;
	auto res = getFractional(params[key], ok);
	if (ok)
		return res;
	//the type of this value is not numeric -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, Float expected" << std::endl;
	exit(1);
}

template<>
inline int JsonParamsHandler::getValue(const std::string& key) const {
	bool ok = false;
	int res = params[key].ToInt(ok);
	if (ok) {
		return res;
	}
	//the type of this value is not Int -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, Int expected" << std::endl;
	exit(1);
}

template<>
inline size_t JsonParamsHandler::getValue(const std::string& key) const {
	bool ok = false;
	int res = params[key].ToInt(ok);
	if (ok && res > 0) {
		return size_t(res);
	}

	if (res < 0) {
		std::cerr << "ERROR: The parameter: " << key << " found in json, but is negative number" << std::endl;
		exit(1);
	}

	//the type of this value is not Int -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, Int expected" << std::endl;
	exit(1);
}

template<>
inline std::string JsonParamsHandler::getValue(const std::string& key) const {
	bool ok = false;
	std::string res = params[key].ToString(ok);
	if (ok) {
		return res;
	}
	//the type of this value is not string -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, String expected" << std::endl;
	exit(1);
}

template<>
inline bool JsonParamsHandler::getValue(const std::string& key) const {
	bool ok = false;
	bool res = params[key].ToBool(ok);
	if (ok) {
		return res;
	}
	//the type of this value is not string -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, String expected" << std::endl;
	exit(1);
}

} //end of namespace json_params

#endif /* JSON_PARAMS_H_ */
