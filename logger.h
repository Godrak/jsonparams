/*
 * logger.h
 *
 *  Created on: Apr 22, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <sstream>
#include <string>
#include <iomanip>
#include <fstream>
#include <iostream>

class Logger {
public:
    Logger(bool console_output) {
        console = console_output;
    }

    Logger(std::string filename, bool console_output) {
        file_stream.open(filename, std::ofstream::out | std::ofstream::app);
        file = true;
        console = console_output;
    }

    template<typename T>
    Logger& operator<<(const T& data) {
        if (console) {
            std::cout << data;
        }
        if (file) {
            file_stream << data;
            file_stream.flush();
        }

        return *this;
    }

    Logger& operator<<(std::ostream& (*f)(std::ostream&)) {
        if (console) {
            std::cout << f;
        }
        if (file) {
            file_stream << f;
            file_stream.flush();
        }
        return *this;
    }

    void close() {
        if (console) {
            std::cout << std::endl;
        }
        if (file) {
            file_stream << std::endl;
            file_stream.close();
        }

        file = false;
        console = false;
    }

    ~Logger() {
        close();
    }

protected:
    bool console = false;
    bool file = false;
    std::ofstream file_stream;
};

#endif /* LOGGER_H_ */
