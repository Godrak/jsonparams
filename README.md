# jsonParams

Simple C++ header-only library for parsing program parameters from json file. 

## Example of param definition:

	struct outputDirectory {
		static constexpr const char* name = "outputDirectory"; //entry to look for in json input file
		static constexpr const char* desc = "some descripiton"; // description of the parameter
		using Type = std::string;  // Type that should be parsed. Custom Types are supported
		static Type getDefault() {  // Default value.. can be either value of type Type, or use exit when default value is not supported.
			std::cerr << "ERROR: no default value for parameter: " << name << std::endl;
			exit(1);
		}
	};

You can also use macros **PARAM** and **PARAMWD** to define parameters, e.g. the above definition would be:
	
	PARAM(outputDirectory, std::string, "some descripiton");

Then to load the config file and get the param value:

	json_params::JsonParamsHandler q { "test" };  // loads file "test", which should be valid json file
	std::cout << q.get<outputDirectory>() << std::endl;  // retrieval of the parameter value for outputDirectory, defined previously.


The library also allows setting param values during runtime and dumping the json file with current params, useful for 
creating a default/initial config files, see main.cpp for usage example.

## Type extensions
To extend the library parsing ability to another type, extend the **json_params** namespace with another 
implementation of **JsonParamsHandler::getValue** function like this:

	template<>
	inline MyType JsonParamsHandler::getValue(const std::string& key) const {
		bool ok = false;
		*** json_val = params[key][0].To****(ok); // you can extract any json type from the json here - json object, fractional, array, etc.
		MyType res = MyParsingFunc(json_val, ok);
		if (ok)
			return res;
		//the type of this value is not MyType -> error
		std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, MyType expected" << std::endl;
		exit(1);
	}
 And thats it :) 



Additionaly added a simple logger class, which can log into file, standard output, or both.