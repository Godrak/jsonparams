/*
 * jparam_structs.h
 *
 *  Created on: Jun 10, 2020
 *      Authors: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef JPARAM_STRUCTS_H_
#define JPARAM_STRUCTS_H_

#include<iostream>

#define PARAM(NAME, TYPE, DESC)  struct NAME{                                               \
	static constexpr const char* name = #NAME;                                              \
	static constexpr const char* desc = DESC;                                               \
	using Type = TYPE;                                                                      \
	static Type getDefault() {                                                              \
		std::cerr << "ERROR: no default value for parameter: " << name << std::endl;        \
		exit(1);                                                                            \
	}                                                                                       \
};

#define PARAMWD(NAME, TYPE, DEFAULT, DESC)  struct NAME{                                    \
	static constexpr const char* name = #NAME;                                              \
	static constexpr const char* desc = DESC;                                               \
	using Type = TYPE;                                                                      \
	static Type getDefault() {                                                              \
		std::cout << "WARNING: picking default value for parameter: " << name << std::endl; \
		return DEFAULT;                                                                     \
	}                                                                                       \
};

namespace json_params {
namespace examples {

struct outputDirectory {
	static constexpr const char* name = "outputDirectory";
	static constexpr const char* desc = "outputDirectory";
	using Type = std::string;
	static Type getDefault() {
		std::cerr << "ERROR: no default value for parameter: " << name << std::endl;
		exit(1);
	}
};

struct oversamplingFactor {
	static constexpr const char* name = "oversamplingFactor";
	static constexpr const char* desc = "oversamplingFactor";
	using Type = int;
	static Type getDefault() {
		return 1;
	}
};

PARAM(count, int, "count of something");

PARAMWD(size, float, 10.0f, "size of something");

}
} //end of namespace jparam

#endif /* JPARAM_STRUCTS_H_ */
