#include "json_params.h"

int main() {

	json_params::JsonParamsHandler p { "hello" };
	p.set<json_params::examples::outputDirectory>("hello");
	p.set<json_params::examples::oversamplingFactor>(0.1f);

	p.set<json_params::examples::count>(1);

	p.set<json_params::examples::size>(5.0f);

	p.dump("test");

	json_params::JsonParamsHandler q { "test" };
	std::cout << q.get<json_params::examples::outputDirectory>() << std::endl;

	std::cout << q.get<json_params::examples::oversamplingFactor>() << std::endl;

	return 0;
}
